function snapCrackle(maxValue) {
    for (let i = 1; i <= maxValue; i++) {
        if ((i % 2) != 0 && (i % 5) == 0) {
            console.log(' SnapCrackle, ');
        } else if ((i % 2) != 0) {
            console.log(' Snap, ');
        } else if ((i % 5) == 0) {
            console.log(' Crackle, ');
        } else {
            console.log(i + ', ');
        }
    }
}

snapCrackle(12);

function snapCracklePrime(maxValue) {
    for (let i = 2; i <= maxValue; i++) {
        let prime = true;

        for (let d = 2; d < i; d++) {
            if ((i % d) === 0) {
                prime = false;
                break;
            } 
        } 

        if ((i % 2) != 0 && (i % 5) == 0) {
            if (prime) {
                console.log(' SnapCracklePrime, ');
            } else {
                console.log(' SnapCrackle, ');            
            }            
        } else if ((i % 2) != 0) {
            if (prime) {
                console.log(' SnapPrime, ');
            } else {
                console.log(' Snap, ');            
            }   
        } else if ((i % 5) == 0) {
            if (prime) {
                console.log(' CracklePrime, ');
            } else {
                console.log(' Crackle, ');            
            }   
        } else {
            console.log(i + ', ');
        }
    }          
}

snapCracklePrime(15);